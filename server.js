const express = require("express");
const bodyParser = require("body-parser");
const config = require("config");
const http = require("http");
const server = express();
const cors = require('cors')

// server.use(bodyParser.json());
server.use(cors())
server.use(express.json());

const loginUsersRouter = require("./routes/loginUser").router;
server.use("/api/v1/dalilah/users/login", loginUsersRouter);

const listingUsersRouter = require("./routes/listingUsers").router;
server.use("/api/v1/dalilah/users/list", listingUsersRouter);

const registeringUsersRouter = require("./routes/registeringUser").router;
server.use("/api/v1/dalilah/users/register", registeringUsersRouter);

const listingProductsRouter = require("./routes/listingProducts").router;
server.use("/api/v1/dalilah/products/list", listingProductsRouter);

const addProductsRouter = require("./routes/addProducts").router;
server.use("/api/v1/dalilah/products/add", addProductsRouter);

const deletingProductsRouter = require("./routes/deletingProducts").router;
server.use("/api/v1/dalilah/products/delete", deletingProductsRouter);

const updatingProductsRouter = require("./routes/updatingProducts").router;
server.use("/api/v1/dalilah/products/update", updatingProductsRouter);

// create pedido user , update pedido admin, eliminar pedido admin
const creatingOrdersRouter = require('./routes/creatingOrders').router;
server.use('/api/v1/dalilah/orders/create', creatingOrdersRouter)

const listingOrdersRouter = require('./routes/listingOrders').router;
server.use('/api/v1/dalilah/orders/list', listingOrdersRouter)

const deletingOrdersRouter = require('./routes/deletingOrders').router;
server.use('/api/v1/dalilah/orders/delete', deletingOrdersRouter)

const updatingOrderStatusRouter = require('./routes/updatingOrdersStatus').router;
server.use('/api/v1/dalilah/orders/update', updatingOrderStatusRouter)

http.createServer(server).listen(config.port, () => {
  console.log(`App listening on port ${config.port}`);
});
