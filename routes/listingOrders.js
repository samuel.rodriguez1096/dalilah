const router = require('express').Router();
const { mySqlConnectionObject } = require('../db/connectDB');
const { queries } = require('../config/dbQueries.json')
const { QueryTypes } = require("sequelize");
const authMiddlewares = require("../middlewares/auth");
router.use(authMiddlewares.authorizationMiddleware);
router.use(authMiddlewares.isAdmin);
router.get('/', async (req, res) => {
    mySqlConnectionObject.query(queries.listingOrders, { type: QueryTypes.SELECT })
        .then((result) => {
            return res.status(200).json({
                statusCode: 200,
                message: "Request Successfull",
                orders: result
            })
        }).catch((e) => {
            return res.status(500).json({
                errorCode: 500,
                message: "There was an error when listing the orders",
                errorMessage: e
            })
        })
})
module.exports = {
    router
}