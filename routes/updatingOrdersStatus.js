const router = require('express').Router();
const { mySqlConnectionObject } = require('../db/connectDB');
const { queries } = require('../config/dbQueries.json')
const { QueryTypes } = require("sequelize");
const authMiddlewares = require("../middlewares/auth");
router.use(authMiddlewares.authorizationMiddleware);
router.use(authMiddlewares.isAdmin)
router.put('/', async (req, res) => {
    if ('order_status' && 'order_id' in req.body) {
        if (req.body.order_status === 'making' || req.body.order_status === 'ready' || req.body.order_status ==='delivered') {
            mySqlConnectionObject.query(queries.updatingOrderStatus, {
                replacements: {
                    order_status: req.body.order_status,
                    order_id: req.body.order_id
                },
                type: QueryTypes.UPDATE
            }).then(() => {
                return res.status(200).json({
                    statusCode: 200,
                    message: 'The order weas updated successfully'
                })

            }).catch((error) => {
                return res.status(500).json({
                    errorCode: 500,
                    errorMessage: error,
                    message: 'There was an internal problem while creating the order'
                })
            })
        } else {
            return res.status(400).json({
                ErrorCode: "400",
                ErrorType: "Bad Request",
                message:
                    "The new order status is not correct, check docs",
            });
        }

    } else {
        return res.status(400).json({
            ErrorCode: "400",
            ErrorType: "Bad Request",
            message:
                "There was no information sent in the body, please check the docs",
        });
    }
});
module.exports = {
    router
}