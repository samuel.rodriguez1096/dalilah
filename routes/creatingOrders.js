const router = require('express').Router();
const { mySqlConnectionObject } = require('../db/connectDB');
const { queries } = require('../config/dbQueries.json')
const { QueryTypes } = require("sequelize");
const authMiddlewares = require("../middlewares/auth");
router.use(authMiddlewares.authorizationMiddleware);
router.post('/', async (req, res) => {
    if ('products' && 'user_name' in req.body) {
        let productsInStock;
        let updatingProducts = [];
        let promises = [];
        mySqlConnectionObject.query(queries.selectingProducts, { type: QueryTypes.SELECT }).then((result) => {
            productsInStock = result;
            let newStock;
            let description = '[';
            let total = 0;
            let counter = 0
            req.body.products.forEach(element => {
                if ('product_name' && 'quantity' in element) {
                    productsInStock.forEach(elemento => {
                        if (element.product_name === elemento.product_name && elemento.product_stock >= element.quantity) {
                            total += (element.quantity * elemento.product_price);
                            description += `{  product_name: ${element.product_name},quantity:${element.quantity}},`
                            newStock = elemento.product_stock - element.quantity
                            updatingProducts.push([elemento.product_id, newStock])
                            counter += 1
                        }
                    })
                }
            })
            if (req.body.products.length === counter) {
                description = description.slice(0, -1) + ']';
                console.log(description)
                mySqlConnectionObject.query(queries.creatingOrders, {
                    replacements: {
                        user_name: req.body.user_name,
                        order_status: 'making',
                        order_description: description,
                        order_total: total

                    },
                    type: QueryTypes.INSERT,
                }).then(() => {
                    updatingProducts.forEach(element => {
                        promises.push(mySqlConnectionObject.query(queries.updatingProductStock, {
                            replacements: {
                                product_stock: element[1],
                                product_id: element[0]
                            }, type: QueryTypes.UPDATE
                        }))
                    })
                    Promise.all(promises).then(() => {
                        return res.status(200).json({
                            statusCode: 200,
                            message: 'The order weas created successfully'
                        })

                    }).catch((error) => {
                        return res.status(500).json({
                            errorCode: 500,
                            message: "There was an error while updating product Stock in the DB",
                            errorMessage: error
                        })
                    })

                }).catch((error) => {
                    console.log(error)
                    return res.status(500).json({
                        errorCode: 500,
                        message: "There was an error while creating the order in the DB",
                        errorMessage: error
                    })
                })
            } else {
                return res.status(400).json({
                    errorCode: 400,
                    message: "The order contains either products which dont exist or products out of stock"
                })
            }
        })
            .catch((error) => {
                return res.status(500).json({
                    errorCode: 500,
                    errorMessage: error,
                    message: 'There was an internal problem while creating the order'
                })
            })
    } else {
        return res.status(400).json({
            ErrorCode: "400",
            ErrorType: "Bad Request",
            message:
                "There was no information sent in the body or there was no products field in the json sent, please check the docs",
        });
    }
})





module.exports = {
    router
}