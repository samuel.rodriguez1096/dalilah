# dalilah

Este proyecto corresponde al tercer proyecto del curso, es necesario tener instalado un servidor sql y nodejs previamente en el ordenador donde se ejecutara el proyecto.

Dentro de la carpeta DB del root del proyecto, existe un archivo llamado CreateDB.sql, es necesario ejecutar los comandos en las primeras 5 lineas, estas lineas, crearan la base de datos, crearan un usuario si no existe y le daran los privilegios para poder trabajar sobre la base de datos. Dicha informacion (usuario y contraseña) deben ser almacenados o actualizados en el archivo development.json en la carpeta config para poder comunicar el backend con la base de datos, acto seguido, es necesario ir al root del proyecto y ejectuar npm install, despues, npm run InitializingDB. Este comando creara todas las tablas necesarias dentro de la base de datos.

Despues de los pasos anteriores, se puede volver al archivo SQL para terminar de ejecutar los comandos restantes, estos comandos crearan un primer producto y un usuario administrador. 

A este punto, desde el root del proyecto se puede iniciar el backend ejectuando npm start. Los detalles de los requests, como funcionan, que devuelven, posibles errores, estan documentados en el archivo api.yaml, que se puede copiar y pegar en swagger para verse con mas comodidad.

Agradezco el tiempo usado para corregir este proyecto y estoy atento a feedback

